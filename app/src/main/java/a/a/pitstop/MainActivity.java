package a.a.pitstop;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    GoogleMap map;
    Double mLocationLat;
    Double mLocationLng;
    Toolbar toolbar;
    ImageButton carButton;
    ImageButton bikeButton;
    ImageButton mCalender;
    Boolean isFinishActivity = false;
    ImageView bikeDialog, carDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setup(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                .getMap();
        map.setMyLocationEnabled(true);

        LocationManager locMngr = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Get Current Location
        Location myLocation = locMngr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (myLocation != null) {
            mLocationLat = myLocation.getLatitude();
            mLocationLng = myLocation.getLongitude();
        }

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(mLocationLat, mLocationLng))
                .zoom(12)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        bikeButton = (ImageButton) findViewById(R.id.bikeButton);
        carButton = (ImageButton) findViewById(R.id.carButton);
        mCalender = (ImageButton) findViewById(R.id.calender);
        bikeDialog = (ImageView) findViewById(R.id.bikeDialog);
        carDialog = (ImageView) findViewById(R.id.carDialog);

        bikeButton.setOnClickListener(this);
        carButton.setOnClickListener(this);
        mCalender.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.bikeButton:
                carButton.setClickable(false);
                bikeButton.setClickable(false);
                bikeDialog.setVisibility(View.VISIBLE);
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                bikeButton.setVisibility(View.GONE);
                                carButton.setVisibility(View.GONE);
                                bikeDialog.setVisibility(View.GONE);
                                mCalender.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                },5000);

                break;

            case R.id.carButton:
                bikeButton.setClickable(false);
                carButton.setClickable(false);
                v.setVisibility(View.GONE);
                bikeButton.setVisibility(View.GONE);
                mCalender.setVisibility(View.VISIBLE);
                break;

            case R.id.calender:
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if ((!(bikeButton.isShown()) || !(carButton.isShown())) && mCalender.isShown()) {
            mCalender.setVisibility(View.GONE);
            bikeButton.setVisibility(View.VISIBLE);
            carButton.setVisibility(View.VISIBLE);
            bikeButton.setClickable(true);
            carButton.setClickable(true);

            isFinishActivity = false;
        } else {
            isFinishActivity = true;
        }

        if (isFinishActivity) {
            super.onBackPressed();
        }
    }
}
